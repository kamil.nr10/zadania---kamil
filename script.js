// 1.
const list = [1, 2, 3, 4, 4, 3, 2, 1];

const unique = (value, index, self) => {
  return self.indexOf(value) === index;
};

const uniqueValues = list.filter(unique);
console.log(uniqueValues);

// 2.1
const arraySecond = [{
    a: 1,
    b: 2
  },
  {
    a: 5,
    b: 8
  },
  {
    a: "aaaa",
    c: "aaaaa"
  }
];

const array = arraySecond.map(item => item.a);
console.log(array);

// 2.2
const third = [1, 2, 3, 4];
const arrayThird = third.map(item => {
  return item * 2;
});
console.log(arrayThird);

// 2.3
const fourth = [1, 2, 3, 4, 5, 6];
const fourthArray = fourth.filter(item => {
  if (item % 2 === 0) {
    return item;
  }
});
console.log(fourthArray);

// 2.4
const fifth = [1, 2, 3, 4];
const fifthArray = fifth.reduce((prev, next) => {
  return prev + next;
});
console.log(fifthArray);

// 2.5
const sixth = [1, 2, 3, 4, 5, 6];
const sixthArray = sixth
  .filter(item => {
    if (item % 2 === 1) {
      return item;
    }
  })
  .reduce((prev, next) => {
    return prev + next;
  });
console.log(sixthArray);

// 2.6
const seventh = {
  who: "Organization",
  why: "Harmony",
  how: "Human"
};

const keys = Object.keys(seventh);
const values = Object.values(seventh);
const a1 = Object.create(seventh);
const arr = [];
for (let i = 0; i < keys.length; i++) {
  arr[i] = {
    key: keys[i],
    value: values[i]
  };
}
console.log(arr);

// 2.7
const ki = [{
  name: "animal",
  age: 3
}, {
  name: "human",
  age: 10
}];
console.log(Object.values(ki));
const kimap = ki.map(item => item.name);
console.log(kimap);
const kivalues = ki.map(value => value.age);
console.log(kivalues);

const result = {};
kimap.forEach((key, i) => (result[key] = kivalues[i]));
console.log(result);

// 2.8
const last = {
  key1: 1,
  key2: "tar",
  key3: [{
      name: "animal",
      age: 3
    },
    {
      name: "human",
      age: 10
    }
  ]
};

const eightKeys = Object.keys(last);
// console.log(eightKeys);
const eightValues = Object.values(last);
// console.log(eightValues);
const arrEight = [];
for (let i = 0; i < keys.length; i++) {
  arrEight[i] = {
    key: eightKeys[i],
    value: eightValues[i]
  };
}
console.log(arrEight);

// 3
const arrayOfIp = ["1.1.1.1", "2.2.2.2", "3.3.3.3", "4.4.4.4"];
const emptyArray = [];
for (let i = 0; i < arrayOfIp.length; i++) {
  axios.get(`http://ip-api.com/json/${arrayOfIp[i]}`).then(response => {
    // console.log(response.data);
    emptyArray[i] = {
      query: response.data.query,
      isp: response.data.isp,
      city: response.data.city
    };
  });
}
console.log(emptyArray);


// Jeżeli chodzi o zadania 2.6, 2.7, 2.8 i 3 - to widać od razu że nie do końca udało Ci się zgłębić programowanie funkcjonalne.

// 1. W 2.6 można zrobić prosto Object.keys(seventh).map((key)=>{return {key, value: seventh[key]}});

// 2. W 2.6, 2.7 i 3 mutujesz obiekt result mimo że jest const, co nie jest specjalnie dobrą praktyką.

// 3. Używasz też sporo pętli "for" która nie jest przydatna w momencie gdy mam map i foreach. 